### Include component

`common/config/main-local.php`

```
'components' => [
    ...
    'coreApi' => [
        'class' => 'usolutions\core\api\CoreApi',
        'server' => 'http://api.u-solutions.dev/v2',
        'apiToken' => 'API_TOKEN',
    ],
    ...
],
```

### Use

```
Yii::$app->coreApi->get('users');
Yii::$app->coreApi->delete('promo/codes/4');

$data = [
    'param1' => 'value1',
    'param2' => 'value2',
];

Yii::$app->coreApi->post('promo/codes', $data);
Yii::$app->coreApi->put('promo/codes/1', $data);
```

If you need multi languages support, then you can use construction bellow:

```
Yii::$app->coreApi->setLanguage('ru')->get('users');
Yii::$app->coreApi->setLanguage('uk-UA')->delete('promo/codes/4');
```
