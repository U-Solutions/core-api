<?php

namespace usolutions\core\api;

use Exception;

/**
 * Class CoreApi
 * @todo delete apiName & apiPassword
 * @package usolutions\core\api
 */
class CoreApi
{
    /**
     * Support methods
     */
    const METHOD_GET = 'get';
    const METHOD_POST = 'post';
    const METHOD_PUT = 'put';
    const METHOD_DELETE = 'delete';

    /**
     * @var string API URL
     */
    public $server;

    /**
     * @var string API name
     */
    public $apiName;

    /**
     * @var string API password
     */
    public $apiPass;

    /**
     * @var string
     */
    public $apiToken;

    /**
     * Timeout on request
     * @var integer
     */
    public $timeout = 20;

    /**
     * @var boolean
     */
    public $verifySSL = false;

    /**
     * @var integer
     */
    public $verifyHost = 2;

    /**
     * @var string
     */
    private $_method;

    /**
     * @var string
     */
    private $_request;

    /**
     * @var array
     */
    private $_data;

    /**
     * @var string Content language
     */
    private $_language = 'en-US';
    
    /**
     * Make get request
     * @param string $request API request
     * @param array $data Request params
     * @return string
     * @throws Exception
     */
    public function get($request, $data = [])
    {
        $this->_method = self::METHOD_GET;

        if (!empty($data)) {
            $request .= '?' .  http_build_query($data);
        }

        $this->_request = $request;

        return $this->send();
    }

    /**
     * Make post request
     * @param string $request API request
     * @param array $data Request params
     * @return string
     * @throws Exception
     */
    public function post($request, $data)
    {
        $this->_method = self::METHOD_POST;
        $this->_data = $data;
        $this->_request = $request;

        return $this->send();
    }

    /**
     * Make put request
     * @param string $request API request
     * @param array $data Request params
     * @return string
     * @throws Exception
     */
    public function put($request, $data)
    {
        $this->_method = self::METHOD_PUT;
        $this->_data = $data;
        $this->_request = $request;

        return $this->send();
    }

    /**
     * Make delete request
     * @param string $request API request
     * @return string
     * @throws Exception
     */
    public function delete($request)
    {
        $this->_method = self::METHOD_DELETE;
        $this->_request = $request;

        return $this->send();
    }


    /**
     * Send request
     * @return object
     * @throws Exception
     */
    public function send()
    {
        if (!function_exists('curl_version')) {
            throw new Exception('To work correctly, you need to install cURL library - https://php.net/curl');
        }

        $ch = curl_init($this->server . '/' . $this->_request);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json', 'Accept-Language: ' . $this->_language]);

        $ch = $this->setCurlMethod($ch);
        $ch = $this->setCurlPostData($ch);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->verifySSL);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $this->verifyHost);

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        if ($this->apiToken !== null) {
            curl_setopt($ch, CURLOPT_USERPWD, $this->apiToken . ':');
        } else {
            curl_setopt($ch, CURLOPT_USERPWD, $this->apiName . ':' . $this->apiPass);
        }

        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $response = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $curlInfo = curl_getinfo($ch);

        $curl_err = curl_error($ch);

        if ($response === false) {
            curl_close($ch);
            throw new Exception(var_export($curl_err, 1));
        }

        if (!in_array($httpCode, $this->getSuccessHttpCodeList())) {
            curl_close($ch);
            throw new Exception(var_export($response, 1) . "\n" . var_export($curl_err, 1) . "\n" . var_export($curlInfo, 1));
        }

        $response = trim(substr($response, $curlInfo['header_size']));
        curl_close($ch);

        return $response;
    }

    /**
     * Set content language
     * @param string $language Language
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->_language = $language;
        return $this;
    }

    /**
     * Set access token
     * @param string $token
     * @return $this
     */
    public function setAuthToken($token)
    {
        $this->apiToken = $token;
        return $this;
    }

    /**
     * Set params for base authentication
     * @param string $apiName
     * @param string $apiPass
     * @return $this
     */
    public function setAuthParams($apiName, $apiPass)
    {
        $this->apiName = $apiName;
        $this->apiPass = $apiPass;
        return $this;
    }

    /**
     * Get success HTTP codes
     * @return array
     */
    protected function getSuccessHttpCodeList()
    {
        return [200, 201, 204, 302,  401, 403, 404, 405, 422];
    }

    /**
     * Set POST data
     * @param resource $ch
     * @return resource
     * @return mixed
     */
    protected function setCurlPostData($ch)
    {
        if (!empty($this->_data)) {
            if ($this->_method === self::METHOD_PUT) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->_data));
            } else {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_data);
            }
        }

        return $ch;
    }

    /**
     * Set CURL method
     * @param resource $ch
     * @return resource
     * @throws Exception
     */
    protected function setCurlMethod($ch)
    {
        if (!in_array($this->_method, array_keys($this->methodMap()))) {
            throw new Exception();
        }

        return call_user_func_array([$this, $this->methodMap()[$this->_method]], [$ch]);
    }

    /**
     * Get methods map
     * @return array
     */
    protected function methodMap()
    {
        return [
            self::METHOD_GET => 'setCurlGet',
            self::METHOD_POST => 'setCurlPost',
            self::METHOD_PUT => 'setCurlPut',
            self::METHOD_DELETE => 'setCurlDelete',
        ];
    }

    /**
     * Create GET request
     * @param resource $ch
     * @return resource
     */
    protected function setCurlGet($ch)
    {
        return $ch;
    }

    /**
     * Create POST request
     * @param resource $ch
     * @return resource
     */
    protected function setCurlPost($ch)
    {
        curl_setopt($ch, CURLOPT_POST, true);
        return $ch;
    }

    /**
     * Create PUT request
     * @param resource $ch
     * @return resource
     */
    protected function setCurlPut($ch)
    {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        return $ch;
    }

    /**
     * Create DELETE request
     * @param resource $ch
     * @return resource
     */
    protected function setCurlDelete($ch)
    {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
        return $ch;
    }
}
